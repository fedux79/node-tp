const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const cors = require('cors');

mongoose.connect(process.env.url_mongo,{useNewUrlParser: true})
.then(() => {console.log(process.env.Mensaje)}, err=>{console.log(err)});

app.use(cors());
/*
app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS'){
      res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, PATCH');
      return res.status(200).json({

      });
    }  
    next(); 
});*/



const userRoutes  = require('./api/routes/users');
const movimientoRoutes  = require('./api/routes/movimientos');


app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());




app.use('/user',userRoutes);
app.use('/movimientos',movimientoRoutes);

//Esta funcion se ejecutara cuando ninguna de las rutas anteriores machee

app.use((req, res, next)=>{
      const error = new Error('Not Found');
      error.status =404;
      next(error);
});

app.use((error, req, res, next)=>{
  res.status(error.status|| 500);
  res.json({
    error:{
      message: error.message
    }
  });
});

module.exports = app;
