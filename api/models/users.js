const mongoose = require('mongoose');

const cuentaSchema = mongoose.Schema({
      _id: mongoose.Types.ObjectId,
      numero: {type: Number,requiered: true},
      moneda: {type: String, requiered: true},
      estado: {type: Boolean,requiered:true, default:true},
      saldo: {type:Number, requiered:true, default: 0}

});

const userSchema = mongoose.Schema({
     _id: mongoose.Types.ObjectId,
    email:{ type: String,requiered: true},
    password: { type: String, requiered: true},
    cuentas:[cuentaSchema]
});

module.exports = mongoose.model('User',userSchema);