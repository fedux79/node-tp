const mongoose = require('mongoose');

const movimientoSchema = mongoose.Schema({
      _id: mongoose.Types.ObjectId,
      emailO: {type: String, requiered: true},
      emailD: {type: String, requiered: true},
      monto:  {type:Number, requiered:true},
      moneda: {type:String, requiered:true, default: 0},
      descipcion: {type: String},
      time : { type : Date, default: Date.now }
});

module.exports = mongoose.model('Movimiento',movimientoSchema);