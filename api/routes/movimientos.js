const express = require('express');
const router = express.Router();
const Movimiento = require('../models/movimientos');
const User =require('../models/users');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const checkAuth = require('../middleware/check-auth');




router.post('/',(req, res, next)=>{
    //valido que vengan en el post los datos  mail origen y dentino y monto
    const emailO = req.body.emailo;
    const emailD = req.body.emaild;
    const monto = req.body.monto;
    const moneda = req.body.moneda;
    var indice = 0;
    //console.log(emailD+" "+emailO+" "+monto+" "+moneda);
   
    if (emailD.length <3 || emailO.length <3 || isNaN(monto) || moneda.length <3){
        return res.status(500).json({
            message: "Error en datos informados"
        });
    }
    if (moneda.toUpperCase() === "PESOS"){
         indice = 0;
    }
    if (moneda.toUpperCase() === "DOLAR"){
         indice = 1;
    }  
//Busco el cliente origen de la cuenta
    User.findOne({email:emailO})
    .exec()
    .then(usero=>{
         // resto monto de cuenta origen
           usero.cuentas[indice].set({saldo:usero.cuentas[indice].saldo - monto})
           usero.save()
           .then(console.log("Se debito el monto correctamente"))
           .catch(err=> {console.log(err)
                   res.status(500).json({ 
                    message: "error al actualizar montos"
                })
              });
    })
    .catch(err=> {console.log(err)
      res.status(500).json({ 
       message: "Origen de datos fondos no enocntrado"
   })
 })
 
 //Busco el cliente destino de la cuenta

 User.findOne({email:emailD})
.exec()
.then(userd=>{
      
     // resto monto de cuenta origen
     userd.cuentas[indice].set({saldo:userd.cuentas[indice].saldo + monto})
     userd.save()
       .then(console.log("Se acredito el monto correctamente"))
       .catch(err=> {console.log(err)
        res.status(500).json({ 
         message: "error al actualizar montos"
     })
   });
})
.catch(err=> {console.log(err)
  res.status(500).json({ 
   message: "Destinatario no encontrado"
})
})

//Genero el registro del movimiento

    const movimiento = new Movimiento({
      _id: new mongoose.Types.ObjectId(),
      emailO: req.body.emailo,
      emailD: req.body.emaild,
      monto:  monto,
      moneda: moneda,
      descipcion:req.body.descripcion
    })
    movimiento.save()
    .then(mov=>{
             res.status(200).json({
                 message: 'Se genero el movimiento correctamente'
             })
    })
    .catch(err=> {console.log(err)
      res.status(500).json({ 
       message: "error al generar registro de movimientos"
   })
 })
  }
);


router.get('/:email',(req,res,next)=>{
  //Movimiento.find({emailO:req.params.email})
  Movimiento.find({$or:[{emailO:req.params.email },{emailD:req.params.email}]})
  .exec()
  .then(mov=>{
    if(mov.length>=1){
        res.status(200).json(mov)
    }
    else{
      res.status(404).json({
        message: "Movimientos no encontrados"
      })
    }
  })
  .catch(err=>{console.log(err)
    res.status(500).json({
      message: "Error al recuperar todos los movimientos"
    })
  })
});


router.get('/',(req,res,next)=>{
    Movimiento.find()
    .exec()
    .then(mov=>{
      if(mov.length>=1){
          res.status(200).json(mov)
      }
      else{
        res.status(404).json({
          message: "Movimientos no encontrados"
        })
      }
    })
    .catch(err=>{console.log(err)
      res.status(500).json({
        message: "Error al recuperar todos los movimientos"
      })
    })
});

module.exports = router;