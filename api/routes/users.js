const express = require('express');
const router = express.Router();
const User = require('../models/users');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const checkAuth = require('../middleware/check-auth');


//update
//pueba borrar
router.put('/cuenta',(req, res, next)=>{
  User.findOne({email:req.body.email})
  .exec()
  .then(docum=>{
/*
        docum.cuentas.push({
              _id: new mongoose.Types.ObjectId(),
              numero:3,
              moneda: "pesos",
              estado:true,
              saldo: 0});*/
        docum.cuentas[0].set({saldo:docum.cuentas[0].saldo +300})
        docum.save()
        .then(result => {
          return res.status(200).json({
            message:'Ususario modificado correctamente'
          })
         })
        .catch(console.log(err));
  })
  .catch(err=>console.log(err));
});

//Login
router.post('/login',(req, res, next)=>{
  User.find({email: req.body.email})
  .exec()
  .then(user=>{
    if (user.length < 1){
      console.log('mail not found')
      return res.status(401).json({
        message: 'Auth failed'
      });
    }
    bcrypt.compare(req.body.password,user[0].password, (err, result)=>{
       if(err){
        return res.status(401).json({
          message: 'Auth failed'
        });
       }
       if(result){
         const token = jwt.sign({
           email: user[0].email,
           userId: user[0]._id
         },
         process.env.JWT_KEY,
         {
           expiresIn: "1h"
         });
         return res.status(200).json({
           message: 'Auth OK',
           token: token
         })
       }
       return res.status(401).json({
        message: 'Auth failed'
         });
  });
})
  .catch(err=>{console.log(err);
          return res.status(500).json({
            error: err
          });
        });
   });



//Alta de clientes
router.post('/', (req, res, next)=>{
  User.find({email: req.body.email})
  .exec()
  .then(user=>{
    if (user.length >= 1){
      console.log('mail not found')
      return res.status(500).json({
        message: 'Usuario existente'
      });
    }
    else{
   bcrypt.hash(req.body.password,10,(err,hash)=>{
         if(err){
           res.status(500).json({
             message: 'Error al encriptar la clave'
           })
         }
         else {
           console.log('ok bcrypt')
           const user = new User({
          _id: new mongoose.Types.ObjectId(),
          email: req.body.email,
          password: hash
          })     
          user.cuentas.push({
            _id: new mongoose.Types.ObjectId(),
            numero:0,
            moneda: "pesos",
            estado:true,
            saldo: 0});
            user.cuentas.push({
              _id: new mongoose.Types.ObjectId(),
              numero:1,
              moneda: "dolar",
              estado:true,
              saldo: 0});
         user.save()
         .then(result => {
           return res.status(200).json({
             message:'Ususario generado correctamente'
           })
            console.log(result);
          })
         .catch(console.log(err)  
         );
        }})}
      })
  });



//Recupera todos los usuarios
router.get('/',checkAuth, (req, res, next)=>{
  User.find()
  .exec()
  .then(doc=>{
    console.log(doc);
    if (doc.length >= 1){
      res.status(200).json(doc)
    }
    else{
      res.status(404).json({
        message: 'Usuario no encontrado'
      })
    }
  })
  .catch(err=> console.log)

});

//recuperar un usuario en particular x email
router.get('/:email',checkAuth,(req, res , next)=>{
  const email = req.params.email;
  User.find({email:email})
  .exec()
  .then(doc=>{
    console.log(doc);
    if (doc.length >= 1){
      res.status(200).json(doc)
    }
    else{
      res.status(404).json({
        message: 'Usuario no encontrado'
      })
    }
  })
  .catch(err=> console.log)
});

//Baja de usuarios
router.delete('/:email',(req, res, next)=>{
  const email = req.params.email;
  User.find({email :email})
  .exec()
  .then(user =>{
      if (user.length < 1){
          res.status(202).json({
             message: 'el email no existe'
        })
      }
      else {
        User.remove({email:email})
        .exec()
        .then(doc=>{
          console.log(doc);
            res.status(200).json(doc)
        
        })
        .catch(err=> console.log)
      }})
  .catch(err => console.log(err))

});


module.exports = router;
